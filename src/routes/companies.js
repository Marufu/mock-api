const express = require('express');
const router = express.Router();
const bookings = require('../bookings');
const companies = require('../companies');
const random = require('../number_helper').random;
const constants = require('../constants');
const tempStorage = [];
const tempStore = [];

function isAdmin(req) {
    return req.user.role === constants.roles.admin;
}

function isUser(req) {
    return req.user.role === constants.roles.user;
}

const validateVehicle = (vehicle) => {
    if (!vehicle) {
        throw new Error("No vehicle found");
    }
    if (!vehicle.summary) {
        throw new Error("Story vehicle missing");
    }
    if (!vehicle.description) {
        throw new Error("Story vehicle missing");
    }
    if (!vehicle.complexity) {
        throw new Error("Vehicle complexity missing");
    }
    if (constants.vehicleComplexity.indexOf(vehicle.complexity) === -1) {
        throw new Error("Invalid complexity given")
    }
    if (!vehicle.type) {
        throw new Error("Vehicle type missing");
    }
    if (constants.vehicleTypes.indexOf(vehicle.type) === -1) {
        throw new Error("Invalid vehicle type given")
    }
};



function getAllCompanies() {
    return tempStore.concat(companies);
}


router.get("/", (req, res) => {
    const combinedCompanies = getAllCompanies();
    if (isAdmin(req)) {
        return res.json(combinedCompanies);
    }
    if (isUser(req)) {
        return res.json(combinedCompanies.filter(booking => booking.createdBy === req.user.id));
    }
    res.sendStatus(400);
});

module.exports = router;