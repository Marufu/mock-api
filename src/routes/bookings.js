const express = require('express');
const router = express.Router();
const bookings = require('../bookings');
const companies = require('../companies');
const random = require('../number_helper').random;
const constants = require('../constants');
const tempStorage = [];
const tempStore = [];

function isAdmin(req) {
    return req.user.role === constants.roles.admin;
}

function isUser(req) {
    return req.user.role === constants.roles.user;
}

const validateVehicle = (vehicle) => {
    if (!vehicle) {
        throw new Error("No vehicle found");
    }
    if (!vehicle.summary) {
        throw new Error("Story vehicle missing");
    }
    if (!vehicle.description) {
        throw new Error("Story vehicle missing");
    }
    if (!vehicle.complexity) {
        throw new Error("Vehicle complexity missing");
    }
    if (constants.vehicleComplexity.indexOf(vehicle.complexity) === -1) {
        throw new Error("Invalid complexity given")
    }
    if (!vehicle.type) {
        throw new Error("Vehicle type missing");
    }
    if (constants.vehicleTypes.indexOf(vehicle.type) === -1) {
        throw new Error("Invalid vehicle type given")
    }
};

router.post("/", (req, res) => {
    const vehicle = req.body;
    try {
        validateVehicle(vehicle);
    } catch (e) {
        return res.status(400).json({ "error": e.message });
    }
    vehicle.id = bookings.length + tempStorage.length + 1;
    vehicle.createdBy = req.user.id;
    vehicle.cost = 0;
    vehicle.estimatedHrs = 0;
    tempStorage.push(vehicle);
    res.status(201).json(vehicle);
});

router.get("/:id", (req, res) => {
    const combinedBookings = getAllBookings();
    const id = parseInt(req.params.id);
    const bookingById = combinedBookings.find(booking => booking.id === id);
    if (!bookingById) {
        return res.sendStatus(404);
    }
    if (isAdmin(req)) {
        return res.json(bookingById)
    }
    if (isUser(req)) {
        if (bookingById.bookingInitiatorUserId === req.user.id)
            return res.json(bookingById);
    }
    res.sendStatus(400);
});

router.put("/:id/:status", (req, res) => {
    const combinedStories = getAllBookings();
    const id = parseInt(req.params.id);
    const status = req.params.status;
    if (status !== 'accepted' && status !== 'rejected') {
        return res.sendStatus(400)
    }
    const bookingById = combinedStories.find(booking => booking.id === id);
    if (!bookingById) {
        return res.sendStatus(404);
    }
    if (isAdmin(req)) {
        bookingById.status = status
        return res.json(bookingById);
    }
    if (isUser(req)) {
        res.sendStatus(403)
    }
    res.sendStatus(400);
});

function getAllBookings() {
    return tempStorage.concat(bookings);
}

router.get("/", (req, res) => {
    const combinedBookings = getAllBookings();
    if (isAdmin(req)) {
        return res.json(combinedBookings);
    }
    if (isUser(req)) {
        return res.json(combinedBookings.filter(booking => booking.createdBy === req.user.id));
    }
    res.sendStatus(400);
});

module.exports = router;